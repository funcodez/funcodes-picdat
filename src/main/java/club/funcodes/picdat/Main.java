// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.picdat;

import static org.refcodes.cli.CliSugar.*;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.EnumOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.IntOption;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.data.FilenameExtension;
import org.refcodes.exception.BugException;
import org.refcodes.graphical.ColorDepth;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.numerical.NumericalUtility;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.runtime.Execution;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.VerboseTextBuilder;

/**
 * A minimum REFCODES.ORG enabled command line interface (CLI) application. Get
 * inspired by "https://bitbucket.org/funcodez".
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "picdat";
	private static final String TITLE = "PIC👓DAT";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES | See [https://www.metacodes.pro/manpages/picdat_manpage]";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String DESCRIPTION = "Tool for converting (raw) data to a pixmap image (\"[pic]ture[dat]a\") and extracting raw (pixmap) data from an image (see [https://www.metacodes.pro/manpages/picdat_manpage]).";
	private static final String HEIGHT_PROPERTY = "height";
	private static final String WIDTH_PROPERTY = "width";
	private static final String OUTPUTFILE_PROPERTY = "outputFile";
	private static final String INPUTFILE_PROPERTY = "inputFile";
	private static final String COLOR_DEPTH_PROPERTY = "colorDepth";
	private static final String CONVERT_PROPERTY = "convert";
	private static final String EXTRACT_PROPERTY = "extract";
	private static final String OFFSET_PROPERTY = "offset";

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		final Flag theConvertFlag = flag( 'c', "convert", CONVERT_PROPERTY, "Converts given data into an image." );
		final Flag theExtractFlag = flag( 'e', "extract", EXTRACT_PROPERTY, "Extracts pixel data from an image." );
		final StringOption theInputFileArg = stringOption( 'i', "input-file", INPUTFILE_PROPERTY, "The input file which to process from." );
		final StringOption theOutputFileArg = stringOption( 'o', "output-file", OUTPUTFILE_PROPERTY, "The output file which to process to." );
		final IntOption theWidthArg = intOption( "width", WIDTH_PROPERTY, "The width for the targeted image." );
		final IntOption theHeightArg = intOption( "height", HEIGHT_PROPERTY, "The height for the targeted image." );
		final IntOption theOffsetArg = intOption( "offset", OFFSET_PROPERTY, "The offset where to start interpreting the data as image data (e.g. the number of bytes to skip, first byte is at offset 0)." );
		final EnumOption<ColorDepth> theColorDepthArg = enumOption( "color-depth", ColorDepth.class, COLOR_DEPTH_PROPERTY, "The color depth of the targeted image: " + VerboseTextBuilder.asString( ColorDepth.values() ) );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();

		// @formatter:off
		final Term theArgsSyntax = cases(
			and(
				xor( and( theConvertFlag, theWidthArg, theHeightArg, any( theColorDepthArg, theOffsetArg ) ), theExtractFlag ), theInputFileArg, theOutputFileArg, any(  theVerboseFlag )
			),
			and(
				xor( and( theConvertFlag, theWidthArg, theHeightArg, any( theColorDepthArg, theOffsetArg ) ), theExtractFlag ), xor( theInputFileArg, and( theOutputFileArg, any ( theVerboseFlag) ) )
			),
			xor(
				theHelpFlag, and( theSysInfoFlag, any ( theVerboseFlag ) )
			)
		);
		final Example[] theExamples = examples(
			example( "Extract a pixmap (image) file's (raw) data to a file", theExtractFlag, theInputFileArg, theOutputFileArg, theVerboseFlag),
			example( "Convert a data file to a pixmap", theConvertFlag, theInputFileArg, theOutputFileArg, theWidthArg, theHeightArg, theVerboseFlag),
			example( "Convert a data file to a pixmap with given color depth", theConvertFlag, theInputFileArg, theOutputFileArg, theWidthArg, theHeightArg, theColorDepthArg, theVerboseFlag),
			example( "Convert a data file starting at given offset to a pixmap", theConvertFlag, theInputFileArg, theOutputFileArg, theWidthArg, theHeightArg, theOffsetArg, theVerboseFlag),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		final boolean isVerbose = theVerboseFlag.isEnabled();

		InputStream theInputStream = Execution.toBootstrapStandardIn();
		OutputStream theOutputStream = Execution.toBootstrapStandardOut();

		try {
			final int theWidth = theArgsProperties.getIntOr( theWidthArg, -1 );
			final int theHeight = theArgsProperties.getIntOr( theHeightArg, -1 );
			final int theOffset = theArgsProperties.getIntOr( theOffsetArg, 0 );
			if ( theOffset < 0 ) {
				throw new IllegalArgumentException( "The provided offset <" + theOffset + "> for the offset argument <" + theOffsetArg + "> must be >= 0 !" );
			}
			final String theInputFilePath = theArgsProperties.get( theInputFileArg );
			final String theOutputFilePath = theArgsProperties.get( theOutputFileArg );
			final boolean isConvert = theArgsProperties.getBoolean( theConvertFlag );
			final boolean isExtract = theArgsProperties.getBoolean( theExtractFlag );
			final String theFileNameExtension = FilenameExtension.toRawFileNameExtension( theOutputFilePath );
			ColorDepth theColorDepth = theArgsProperties.getEnum( ColorDepth.class, theColorDepthArg );

			if ( isVerbose ) {
				if ( theWidth != -1 ) {
					LOGGER.info( "Pixmap width = " + theWidth );
				}
				if ( theHeight != -1 ) {
					LOGGER.info( "Pixmap height= " + theHeight );
				}
				if ( theColorDepth != null ) {
					LOGGER.info( "Color depth = " + theColorDepth );
				}
				if ( theOffset != 0 ) {
					LOGGER.info( "Offset = " + theOffset );
				}
				LOGGER.info( "Operation = " + ( isConvert ? "Converting" : ( isExtract ? "Extracting" : "?" ) ) );
			}

			if ( theInputFilePath != null && theInputFilePath.length() != 0 ) {
				File theInputFile = new File( theInputFilePath );
				if ( isVerbose ) {
					LOGGER.info( "Input file = \"" + theInputFilePath + "\" (<" + theInputFile.getAbsolutePath() + ">)" );
				}
				if ( !theInputFile.exists() || !theInputFile.isFile() ) {
					throw new FileNotFoundException( "No file \"" + theInputFilePath + "\" (<" + theInputFile.getAbsolutePath() + ">) found!" );
				}
				theInputStream = new FileInputStream( theInputFile );
			}
			theInputStream = new BufferedInputStream( theInputStream );

			if ( theOutputFilePath != null && theOutputFilePath.length() != 0 ) {
				File theOutputFile = new File( theOutputFilePath );
				if ( isVerbose ) {
					LOGGER.info( "Output file = \"" + theOutputFilePath + "\" (<" + theOutputFile.getAbsolutePath() + ">)" );
				}
				theOutputStream = new FileOutputStream( theOutputFile );
			}
			theOutputStream = new BufferedOutputStream( theOutputStream );

			// -----------------------------------------------------------------
			// CONVERT DATA INTO PIXMAP
			// -----------------------------------------------------------------

			if ( isConvert ) {
				// Offset |-->
				if ( theOffset > 0 ) {
					long theSkipped = theInputStream.skip( theOffset );
					if ( theSkipped != theOffset ) {
						throw new IOException( "Can only skip <" + theSkipped + "> of the specified (<" + theOffsetArg + ">) <" + theOffset + " bytes!" );
					}
				}
				// Offset <--|

				if ( theColorDepth == null ) {
					theColorDepth = ColorDepth.AWT_COLOR_24_BIT;
				}
				int theBufferType;
				switch ( theColorDepth ) {
				case HIGH_COLOR_16_BIT:
					theBufferType = BufferedImage.TYPE_INT_RGB;
					break;
				case MONOCHROME_1_BIT:
					theBufferType = BufferedImage.TYPE_BYTE_BINARY;
					break;
				case GRAYSCALE_8_BIT:
					theBufferType = BufferedImage.TYPE_BYTE_GRAY;
					break;
				case TRUE_COLOR_24_BIT:
					theBufferType = BufferedImage.TYPE_INT_RGB;
					break;
				case TRUE_COLOR_32_BIT:
					theBufferType = BufferedImage.TYPE_INT_ARGB;
					break;
				default:
					theBufferType = BufferedImage.TYPE_INT_RGB;
					break;
				}

				final BufferedImage theImage = new BufferedImage( theWidth, theHeight, theBufferType );
				byte[] eReads = new byte[theColorDepth.getBytes()];
				out: {
					for ( int y = 0; y < theHeight; y++ ) {
						for ( int x = 0; x < theWidth; x++ ) {
							for ( int i = 0; i < eReads.length; i++ ) {
								eReads[i] = 0;
							}
							if ( theInputStream.read( eReads ) == -1 ) {
								break out;
							}
							switch ( theColorDepth ) {
							case MONOCHROME_1_BIT:
								for ( int i = 0; i < Byte.SIZE; i++ ) {
									theImage.setRGB( x, y, ColorDepth.AWT_COLOR_24_BIT.toColor( NumericalUtility.isBitSetAt( (int) eReads[0], i ) ? 1 : 0, theColorDepth ) );
									x++;
									if ( x >= theWidth ) {
										x = 0;
										y++;
										if ( y >= theHeight ) {
											break out;
										}
									}
								}
								break;
							case GRAYSCALE_8_BIT:
								theImage.setRGB( x, y, ColorDepth.AWT_COLOR_24_BIT.toColor( eReads[0], theColorDepth ) );
								break;
							case MSX_8_BIT:
								theImage.setRGB( x, y, ColorDepth.AWT_COLOR_24_BIT.toColor( eReads[0], theColorDepth ) );
								break;
							case HIGH_COLOR_16_BIT:
								theImage.setRGB( x, y, ColorDepth.AWT_COLOR_24_BIT.toColor( NumericalUtility.toInt( eReads ), theColorDepth ) );
								break;
							case TRUE_COLOR_24_BIT:
								theImage.setRGB( x, y, NumericalUtility.toInt( eReads ) );
								break;
							case AWT_COLOR_24_BIT:
								theImage.setRGB( x, y, NumericalUtility.toInt( eReads ) );
								break;
							case TRUE_COLOR_32_BIT:
								theImage.setRGB( x, y, NumericalUtility.toInt( eReads ) );
								break;
							default:
								break;
							}
						}
					}
				}
				ImageIO.write( theImage, theFileNameExtension, theOutputStream );
			}
			// -----------------------------------------------------------------
			// EXTRACT DATA FROM PIXMAP 
			// -----------------------------------------------------------------
			else if ( isExtract ) {
				final BufferedImage theImage = ImageIO.read( theInputStream );

				if ( isVerbose ) {
					LOGGER.info( "Width = " + theImage.getWidth() );
					LOGGER.info( "Height = " + theImage.getHeight() );
				}

				//	int[] ePixel = null;
				//	byte[] eBytes;
				int ePixel;
				boolean isMoochrome = theImage.getType() == BufferedImage.TYPE_BYTE_GRAY;
				for ( int y = 0; y < theImage.getHeight(); y++ ) {
					for ( int x = 0; x < theImage.getWidth(); x++ ) {
						//	ePixel = theImage.getRaster().getPixel( x, y, ePixel );
						//	for ( int i = 0; i < ePixel.length; i++ ) {
						//		eBytes = NumericalUtility.toBytes( ePixel[i] );
						//		for ( int j = 0; j < eBytes.length; j++ ) {
						//			theOutputStream.write( eBytes[j] );
						//		}
						//	}
						ePixel = theImage.getRGB( x, y );
						if ( isMoochrome ) {
							theOutputStream.write( ( ColorDepth.AWT_COLOR_24_BIT.toRedValue( ePixel ) + ColorDepth.AWT_COLOR_24_BIT.toGreenValue( ePixel ) + ColorDepth.AWT_COLOR_24_BIT.toBlueValue( ePixel ) ) / 3 );
						}
						else {
							theOutputStream.write( ColorDepth.AWT_COLOR_24_BIT.toRedValue( ePixel ) );
							theOutputStream.write( ColorDepth.AWT_COLOR_24_BIT.toGreenValue( ePixel ) );
							theOutputStream.write( ColorDepth.AWT_COLOR_24_BIT.toBlueValue( ePixel ) );
						}
					}
				}
			}
			// -----------------------------------------------------------------
			else {
				throw new BugException( "We encountered a bug, none argument was processed." );
			}
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
		finally {
			if ( theInputStream != null ) {
				try {
					theInputStream.close();
				}
				catch ( IOException ignore ) {}
			}
			if ( theOutputStream != null ) {
				try {
					theOutputStream.close();
				}
				catch ( IOException ignore ) {}
			}
		}
	}
}
